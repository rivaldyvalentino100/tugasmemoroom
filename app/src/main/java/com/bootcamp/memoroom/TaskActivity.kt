package com.bootcamp.memoroom

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.bootcamp.memoroom.local.TaskDatabase
import com.bootcamp.memoroom.model.TaskEntity
import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_task.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class TaskActivity : AppCompatActivity() {

    private var mDBTask :  TaskDatabase? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)
        mDBTask = TaskDatabase.getInstance(this)
        setSupportActionBar(tbMemo)

        val list : View = faList
        list.setOnClickListener { view ->
            val i = Intent(this,MainActivity::class.java)
            startActivity(i)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.item_task, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menuSave -> {
                val title = etTitleTask.text.toString()
                val description = etDescriptionTask.text.toString()
                GlobalScope.async {
                    val result =  mDBTask?.TaskDao()?.insertTask(TaskEntity(null,title,description))
                    result?.let {
                        if (it != 0.toLong() ){
                            runOnUiThread {
                                etTitleTask.setText("")
                                etDescriptionTask.setText("")
                                Toast.makeText(this@TaskActivity,"Input Data Succes", Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}