package com.bootcamp.memoroom

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bootcamp.memoroom.local.TaskDatabase
import com.bootcamp.memoroom.model.TaskEntity
import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.activity_task.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class EditTask : AppCompatActivity() {
    val Task: TaskEntity? by lazy {
        intent.getParcelableExtra<TaskEntity>("Task")
    }

    private var mDBTask: TaskDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        Task?.let {
            etTitleEdit.setText(it.title)
            etDescriptionEdit.setText(it.description)
        }

        mDBTask = TaskDatabase.getInstance(this)

        setSupportActionBar(tbEdit)

        val listEdit : View = faEdit
        listEdit.setOnClickListener { view ->
            val i = Intent(this,MainActivity::class.java)
            startActivity(i)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.item_task, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menuSave -> {
                val title = etTitleEdit.text.toString()
                val description = etDescriptionEdit.text.toString()
                GlobalScope.async {
                    val result = mDBTask?.TaskDao()?.updateTask(
                        TaskEntity(
                            Task?.id,
                            title,
                            description
                        )
                    )
                    runOnUiThread {
                        if (result != 0) {
                            etTitleEdit.setText("")
                            etDescriptionEdit.setText("")
                            Toast.makeText(this@EditTask, "Edit Data Succes", Toast.LENGTH_LONG)
                                .show()
                        }
                    }
                }
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}