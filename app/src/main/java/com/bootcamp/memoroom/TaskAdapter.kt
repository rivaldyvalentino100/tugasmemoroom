package com.bootcamp.memoroom

import android.app.AlertDialog
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bootcamp.memoroom.local.TaskDatabase
import com.bootcamp.memoroom.model.TaskEntity
import kotlinx.android.synthetic.main.list_view_task.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class TaskAdapter(val listTask : List<TaskEntity>) : RecyclerView.Adapter<TaskAdapter.ListTaskViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListTaskViewHolder {
        return ListTaskViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_view_task, parent, false))
    }

    override fun getItemCount(): Int {
        return listTask.size
    }

    override fun onBindViewHolder(holder: ListTaskViewHolder, position: Int) {

        listTask.get(position).let {
            holder.title.text = it.title
            holder.description.text = it.description
        }

        holder.itemView.imgbtnEdit.setOnClickListener{
            val intentEditActivity = Intent(it.context, EditTask::class.java)

            intentEditActivity.putExtra("Task", listTask[position])
            it.context.startActivity(intentEditActivity)
        }

        holder.itemView.imgbtnDelete.setOnClickListener{
            AlertDialog.Builder(it.context).setPositiveButton("Yes"){ p0, p1 ->
                val mDb = TaskDatabase.getInstance(holder.itemView.context)

                GlobalScope.async {
                    val result = mDb?.TaskDao()?.deleteTask(listTask[position])

                    (holder.itemView.context as MainActivity).runOnUiThread{
                        if (result != 0){
                            Toast.makeText(it.context, "Data ${listTask[position].id} Delete Succes", Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(it.context, "Data ${listTask[position].id} Deleted Failed", Toast.LENGTH_LONG).show()
                        }
                    }

                    (holder.itemView.context as MainActivity).fetchData()
                }
            }.setNegativeButton("No"){ p0, p1 ->
                p0.dismiss()
            }.setMessage("Are you sure ${listTask[position].id}").setTitle("Delete Confirmation").create().show()
        }
    }

    inner class ListTaskViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val title = view.tvTitleList
        val description = view.tvDescriptionList
    }
}